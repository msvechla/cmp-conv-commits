# cmp-conv-commits

[nvim-cmp](https://github.com/hrsh7th/nvim-cmp) completion source for [conventional commits](https://www.conventionalcommits.org/).

![demo](cmp-conv-commits.png)

## Getting started

Use your favorit plugin manager to install the plugin:

```lua
use 'https://gitlab.com/msvechla/cmp-conv-commits.git'
```

Then setup the cmp source by following these steps:

```lua
require'cmp'.setup {
  sources = {
    { name = 'cmp_conv_commits' }
  }
}

require("cmp_conv_commits").setup()
```

## Custom Types

By default, the following types are supported in the first line of a file (filteype `gitcommit` by default):

```lua
types = { "fix", "feat", "chore", "docs", "style", "refactor", "perf", "test", "ci"},
```

Additionally `BREAKING CHANGE:` is available in all lines, except for the first line.

To setup your custom types, simply add them via the `types` config:

```lua
require("cmp_conv_commits").setup({
    types = {"wip", "ci", "docs"}
})
```
