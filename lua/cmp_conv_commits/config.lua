local M = {
    filetypes = { "gitcommit" },
    types = { "fix", "feat", "chore", "docs", "style", "refactor", "perf", "test", "ci"},
}

return M
