local source = require("cmp_conv_commits.source")

local M = {}

M.setup = function(overrides)
    require("cmp").register_source("cmp_conv_commits", source.new(overrides))
end

return M
