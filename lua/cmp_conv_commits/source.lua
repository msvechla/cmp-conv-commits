local cmp = require('cmp')

local source = {
    config = {},
    filetypes = {},
}

source.new = function(overrides)
    local self = setmetatable({}, {
        __index = source,
    })

    self.config = vim.tbl_extend("force", require("cmp_conv_commits.config"), overrides or {})
    for _, item in ipairs(self.config.filetypes) do
        self.filetypes[item] = true
    end

    return self
end

function source:is_available()
        return self.filetypes["*"] ~= nil or self.filetypes[vim.bo.filetype] ~= nil
end

function source:complete(params, callback)
    local types = {}
    local line = params.context.cursor.line

    -- add all types from the config,
    -- which should be triggered on the first line
    if line == 0 then
        for _, i in pairs(self.config.types) do
            table.insert(types, {
                label = i,
                insertText = i .. "(${1:scope}):",
                word = i .. "(scope):",
                insertTextFormat = cmp.lsp.InsertTextFormat.Snippet,
                kind = cmp.lsp.CompletionItemKind.Text
            })
        end

        callback({ items = types })
        return true
    end

    -- add special types, which should be triggered only if not in the first line
    -- BREAKING CHANGE
    if line > 0 then
        table.insert(types, {label = "BREAKING CHANGE", insertText = "BREAKING CHANGE: ${1:hint}"})

        callback({ items = types })
        return true
    end

    return false
end

function source:get_debug_name()
    return "cmp_conv_commits"
end

return source
